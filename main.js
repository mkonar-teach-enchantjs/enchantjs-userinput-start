/** Respond to right/left keyboad events **/

window.onload = myGame;
enchant();

function myGame() {
    var game = new Core(320, 320);
    game.fps = 30;

    // Preload assets
    game.preload('circle.png');

    // Specify what should happen when the game loads.
    game.onload = function () {
        var circle = new Sprite(48, 48);
        circle.image = game.assets['circle.png'];

        var circleSpeed = 5;  // Number of pixels to move the circle each frame.
        
        game.rootScene.backgroundColor = 'black';
        game.rootScene.addChild(circle);
        
        // Add listener to the circle for the ENTER_FRAME event to move it.
        circle.addEventListener(Event.ENTER_FRAME, function () {
            if (game.input.right) {
                circle.x += circleSpeed;
            } else if (game.input.left) {
                circle.x -= circleSpeed;
            }
        });
    };

    // Start the game.
    game.start();
}
